#!/bin/bash

#Pregunte al usuario por un archivo, si el archivo existe notifique al usuario 
#que sí existe, además si es un archivo de texto plano pregunte al usuario si 
#desea verlo, en caso afirmativo muéstreselo con paginación (less). Repita 
#hasta que el usuario desee salir.

filepath=""
while [ "$filepath" != "q" ]; do
    read -p "Dígame el path a un archivo (q para salir): " filepath
    yesno=""
    if [ "$filepath" == "q" ]; then
        break
    elif [ -f "$filepath" ]; then
        echo "El archivo existe y es un archivo normal."
        if [ -n "$( file "$filepath" | grep "text" )" ]; then
            echo "Además un archivo de texto."
            while true do
                read -r -p "Desea abrir el archivo? (s/n): " yesno

                case $yesno in
                [yY][eE][sS]|[yY]|[sS][iIíÍ]|[sS])
                    less "$filepath"
                    break
                ;;
                [nN][oO]|[nN])
                    echo "0k..."
                    break

                ;;
                *)
                    echo "Entrada inválida..."
                ;;
                esac
            done
        fi
    else
        echo "$filepath no es un archivo normal o no existe."
    fi
done
exit 0