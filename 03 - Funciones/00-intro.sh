#!/bin/bash

BASEDIR=$1

function check_dir {
    if [ ! -d "$1" ]; then
        errcho "$1 no es un directorio. Debe proveer un directorio."
        exit 2
    fi
}

function errcho {
    >&2 echo $@
}

function crear_dir {
  echo "Se crearán los directorios..."
  mkdir a_dir00
  mkdir -v dir01
  mkdir dir02/dir020
  mkdir -p dir02/dir020
  ls -R *dir*
}

function crear_archivos {
    echo "Se crearán los archivos..."
    touch file00.txt
    touch a_dir00/file{01..04}.txt
    ls a_dir*
}

function mover_archivos {
    echo "Se moveran archivos..."
    mv -v a_dir00 dir00
    cd dir02/
    mv dir020 ../dir01
    cd ..
    mv file00.txt dir01/dir020/
    ls -R dir*
}

function copiar_archivos {
    echo "Copiando archivos...."
    cp -r dir01/* dir02/
    ls -R dir01/ dir02/
}

function borrar_archivos {
    echo "Borrando archivos, ignorando rm -rf / ^_^' ..."
    rm -r dir02
    #rm -rf /
    ls -R dir01/ dir02/
}

function archivo_oculto {
    echo "Creando archivo oculto...."
    cd dir00
    touch .archivo_oculto.txt
    ls
    ls -l
    ls -lah
}

check_dir "$BASEDIR"
cd "$BASEDIR"

crear_dir
crear_archivos
mover_archivos
copiar_archivos
borrar_archivos
archivo_oculto

exit 0


