#!/bin/bash

#En un script que reciba una cantidad no definida de parámetros, imprima cada
#uno de esos parámetros en una nueva linea con su número de parámetro 
#posicional.

echo "Lista de parámetros:"

i="1"
while [ "$#" -gt 0 ]; do
    echo "$i - $1"
    i=$[$i+1]
    shift

done

echo "EOF"
exit 0