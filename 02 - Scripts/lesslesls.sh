#! /bin/bash

if [ "$#" -le 0 ]; then
    echo "You must provide a directory."
    exit 1
fi

LSDIR="$1"

if [ -d "$LSDIR" ]; then
    echo "Se listará el directorio '$LSDIR'."
    ls -lR "$LSDIR" | less
    echo "Done."
    exit 0
else
    echo "El parametro '$LSDIR' no es un directorio, no se puede listar."
   exit 1
fi
