#!/bin/bash

# This script makes a backup of my home directory.
cd /home

# This creates the archive
tar cf /var/tmp/home_valarauco.tar valarauco > /dev/null 2>&1

# First remove the old bzip2 file.  Redirect errors because this generates some if the archive
# does not exist.  Then create a new compressed file.
rm /var/tmp/home_valarauco.tar.bz2 2> /dev/null
bzip2 /var/tmp/home_valarauco.tar

# Copy the file to another host
scp /var/tmp/home_valarauco.tar.bz2 backup-server:/opt/backup/valarauco > /dev/null 2>&1

# Make sure log directory exists.
mkdir -p /home/valarauco/log/

# Create a timestamp in a logfile.
date >> /home/valarauco/log/home_backup.log
echo backup succeeded >> /home/valarauco/log/home_backup.log
