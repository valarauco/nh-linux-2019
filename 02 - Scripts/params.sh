#!/bin/bash
if [ "$#" -le 0 ]; then
    echo "You must provide a username."
    exit 1
fi
clear
echo "This is my second script: $0 . Yey!"
echo "Hello $1, your user UID is $(id -u $1) and we are running $(uname -o)"
echo
exit 0


