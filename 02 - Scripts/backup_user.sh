#!/bin/bash
                                                                                                 
# This script makes a backup of my home directory.

# Change the values of the variables to make the script work for you:
BACKUPDIR=/home
BACKUPFILES=valarauco
TARFILE=/var/tmp/home_valarauco.tar
BZIPFILE=/var/tmp/home_valarauco.tar.bz2
SERVER=backup_server
REMOTEDIR=/opt/backup/valarauco
LOGDIR=/home/valarauco/log/
LOGFILE=/home/valarauco/log/home_backup.log

cd $BACKUPDIR

# This creates the archive
tar cf $TARFILE $BACKUPFILES > /dev/null 2>&1
                                                                                                 
# First remove the old bzip2 file.  Redirect errors because this generates some if the archive
# does not exist.  Then create a new compressed file.
rm $BZIPFILE 2> /dev/null
bzip2 $TARFILE

# Copy the file to another host
scp $BZIPFILE $SERVER:$REMOTEDIR > /dev/null 2>&1

# Make sure log directory exists.
mkdir -p $LOGDIR

# Create a timestamp in a logfile.
date >> $LOGFILE
echo backup succeeded >> $LOGFILE
