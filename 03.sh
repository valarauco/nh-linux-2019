#!/bin/bash

#Dada una lista de direcciones IP almacenada en un archivo de texto, extraiga
#y muestra al usuario el último octeto (el último número) de dichas 
#direcciones. Cree una función para verificar que la entrada es una dirección
#IP válida.

function isIP {
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}


for a in $(cat $1); do
    if ! isIP $a; then
        echo "$a no es una IP válida!"
        continue
    fi
    IFS='.'
    a_array=($a)
    IFS=$OIFS
    echo "IP: $a, último octeto: ${a_array[3]}"
done

exit 0