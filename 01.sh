#! /bin/bash

#Pregunte al usuario por un año, debe verificar que el dato sea válido.
#Calcule si es un año bisiesto y notifique al usuario del resultado.
#Extra: calcule el animal del calendario chino que le tocaría a las personas
# nacidas en ese año.

year=0
leapyear="no"
 
read -p "Digite un año: " year

case $year in
    ''|*[!0-9]*) 
        echo "Eso no parece un año!, bye"
        exit 1
    ;;
esac

[ $(($year % 4)) -eq 0 ] && ([ $(($year % 100)) -ne 0 ] || [ $(($year % 400)) -eq 0 ]) && leapyear="sí"
echo "El año $year $leapyear es año bisiesto." ;


ANIMAL=( 
    Mono 
    Gallo 
    Perro 
    Cerdo 
    Rata 
    Buey 
    Tigre 
    Conejo 
    Dragon 
    Serpiente 
    Caballo 
    Cabra )

echo "Y el animal del horoscopo chino: ${ANIMAL[$(($year % 12))]}"
exit 0