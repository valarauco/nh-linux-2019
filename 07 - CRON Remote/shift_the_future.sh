#!/bin/bash

USAGE="Usage: $0 [acción][opciones] archivo.\nPruebe '$0 -h' para más información.\n"

if [ "$#" == "0" ]; then
	echo -e "$USAGE"
	exit 1
fi

function is_command_setup {
    if [ -n "$COMMAND" ]; then
        echo -e "Solo se puede ejecutar una acción a la vez!\n"
        echo -e "$USAGE"
        exit 1
    fi

}

COMMAND=""
while [ True ]; do
    case $1 in
        "-t"|"--tail")
            is_command_setup
            COMMAND="tail "
            if [ "$2" = "-f" ]; then
                COMMAND="${COMMAND} -f"
                shift
            fi
        ;;
        "-l"|"--less")
            is_command_setup
            COMMAND="less "
        ;;
        "-c"|"--cat")
            is_command_setup
            COMMAND="cat "
        ;;
        "-h"|"--help")
            echo "Usage: $0 [acción][opciones] archivo"
            echo
            echo "archivo: el archivo sobre el que se ejecutará la acción."
            echo
            echo "Acciones: "
            echo "  -c|--cat: Muestra el archivo utilizando el comando cat."
            echo "  -l|--less: Muestra el archivo utilizando el comando less."
     #       echo "  -s|--ls: Muestra el archivo utilizando el comando ls. "
            echo "  -t|--tail [-f]: Muestra el archivo utilizando el comando tail. "
            echo "                  Si se utiliza la opción -f se utilizará el comando tail -f."
            echo "  -h|--help: Muestra esta ayuda. "
            echo
            exit 0
        ;;
        -*)
            echo -e "${1}: Acción no reconocida\n"
            echo -e $USAGE
            exit 1
        ;;
        *)
            break
        ;;
    esac
    shift
done


if [ -z "$1" ] || [ ! -f $1 ] || [ "$#" -gt 1 ]; then
    echo -e "El último parámetro debe ser un archivo.\n"
    echo -e $USAGE
    exit 1
fi

echo $COMMAND $1
