#!/bin/bash

#Un script que reciba 3 parámetros: 1) Un texto, determinar si es un 
#palíndromo, 2) Un texto para imprimir en caso de que 1 sea palíndromo, 3) un
#texto para imprimir en caso de que 1 NO sea palíndromo. El script debe 
#retornar un exit code exitoso si es un palíndromo y un código de error si no.
#Extra: ¿Qué beneficio tiene que el script utilice el exit code como salida al
#determinar si es Palíndromo o no?

palabra=$1
afirmativo=$2
negativo=$3

palabra=${palabra//[[:space:]]/}
rev_palabra=$(echo $palabra | rev)

if [[ "$palabra" = "$rev_palabra" ]] ; then
    [[ -n "$afirmativo" ]] && echo "$afirmativo"
    exit 0
else
    [[ -n "$negativo" ]] && echo "$negativo"
    exit 1
fi